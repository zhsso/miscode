// alterkeys.c
// http://osxbook.com
//
// Complile using the following command line:
//     gcc -Wall -o alterkeys alterkeys.c -framework ApplicationServices
//
// You need superuser privileges to create the event tap, unless accessibility
// is enabled. To do so, select the "Enable access for assistive devices"
// checkbox in the Universal Access system preference pane.

#include <ApplicationServices/ApplicationServices.h>
#include <assert.h>
#include <pthread.h>

// This callback will be invoked every time there is a keystroke.
//
int PageUpTimes = 0;
int PageDownTimes = 0;
int pageUpDown(bool isUp)  {
       CGKeyCode keyCode;
       if (isUp){
        keyCode = (CGKeyCode)0x7e;
       }else{
         keyCode = (CGKeyCode)0x7d;
       }
        CGEventSourceRef source = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
        CGEventRef saveCommandDown = CGEventCreateKeyboardEvent(source,keyCode, true);
        CGEventSetFlags(saveCommandDown, kCGEventFlagMaskCommand);
        CGEventRef saveCommandUp = CGEventCreateKeyboardEvent(source, keyCode, false);

        CGEventPost(kCGAnnotatedSessionEventTap, saveCommandDown);
        CGEventPost(kCGAnnotatedSessionEventTap, saveCommandUp);

        CFRelease(saveCommandUp);
        CFRelease(saveCommandDown);
        CFRelease(source);
        return 1;
}

void* pageUpDownThread(void* data){
    bool isUp = *((bool*)data);
    if (isUp){
        while(1){
            if (PageUpTimes > 0){
                for(; PageUpTimes > 0; PageUpTimes--){
                   
                    pageUpDown(isUp);
                }
            }
            usleep(35);
        }
    }else{
        while(1){
            if (PageDownTimes > 0){
                for(; PageDownTimes > 0; PageDownTimes--){
                    pageUpDown(isUp);
                }
               
            }
            usleep(35);
        }
    }
}

void LaunchThread(bool isUp)
{
    // Create the thread using POSIX routines.
    pthread_attr_t  attr;
    pthread_t       posixThreadID;
    int             returnVal;
 
    returnVal = pthread_attr_init(&attr);
    assert(!returnVal);
    returnVal = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    assert(!returnVal);
 
    int     threadError;
    threadError = pthread_create(&posixThreadID, &attr, &pageUpDownThread, &isUp);

 
    returnVal = pthread_attr_destroy(&attr);
    assert(!returnVal);
    if (threadError != 0)
    {
        printf("创建失败\n");
    }else{
        printf("创建成功\n");
    }
}


CGEventRef
myCGEventCallback(CGEventTapProxy proxy, CGEventType type,
                  CGEventRef event, void *refcon)
{
    // The incoming keycode.
    int keycode = CGEventGetIntegerValueField(event, kCGTabletProximityEventSystemTabletID);
    // Set the modified keycode field in the event.
   printf("%d\n", keycode);
   // CGEventSetIntegerValueField( event, kCGKeyboardEventKeycode, (int64_t)keycode);
    return event;
}


int
main(void)
{
    CFMachPortRef      eventTap;
    CGEventMask        eventMask;
    CFRunLoopSourceRef runLoopSource;
   // LaunchThread(true);
    //LaunchThread(false);
    // Create an event tap. We are interested in key presses.
    eventMask = kCGEventMaskForAllEvents;
    eventTap = CGEventTapCreate(kCGSessionEventTap, kCGHeadInsertEventTap, 0,
                                eventMask, myCGEventCallback, NULL);
    if (!eventTap) {
        fprintf(stderr, "failed to create event tap\n");
        exit(1);
    }

    // Create a run loop source.
    runLoopSource = CFMachPortCreateRunLoopSource(
                                                  kCFAllocatorDefault, eventTap, 0);

    // Add to the current run loop.
    CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource,
                       kCFRunLoopCommonModes);

    // Enable the event tap.
    CGEventTapEnable(eventTap, true);

    // Set it all running.
    CFRunLoopRun();

    // In a real program, one would have arranged for cleaning up.

    exit(0);
}
