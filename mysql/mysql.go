package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/coopernurse/gorp"
	_ "github.com/go-sql-driver/mysql"
)

var DBMap *gorp.DbMap //用户数据库
var once = &sync.Once{}

type SpellIntro struct {
	Id          int    `db:"id" json:"id"`
	Lv          int    `db:"lv" json:"lv"`
	IconId      int    `db:"icon_id" json:"icon_id"`
	Name        string `db:"name" json:"name"`
	Intro       string `db:"intro" json:"intro"`
	UpgradeInfo string `db:"upgrade_info" json:"upgrade_info"`
	NameTw      string `db:"name_tw" json:"name_tw"`
	IntroTw     string `db:"intro_tw" json:"intro_tw"`
	UpInfoTw    string `db:"up_info_tw" json:"up_info_tw"`
	NameEn      string `db:"name_en" json:"name_en"`
	IntroEn     string `db:"intro_en" json:"intro_en"`
	UpInfoEn    string `db:"up_info_en" json:"up_info_en"`
	SoundId     int    `db:"sound_id" json:"sound_id"`
	EffectId    int    `db:"effect_id" json:"effect_id"`
	MaxLv       int    `db:"max_lv" json:"max_lv"`
	HeroLv      int    `db:"hero_lv" json:"hero_lv"`
	Gold        int    `db:"gold" json:"gold"`
	Stone       int    `db:"stone" json:"stone"`
	HonorPoint  int    `db:"honor_point" json:"honor_point"`
	GroupPoint  int    `db:"group_point" json:"group_point"`
	ItemId      int    `db:"item_id" json:"item_id"`
	ItemNum     int    `db:"item_num" json:"item_num"`
	UpTime      int64  `db:"up_time" json:"up_time"`
}

func GetByPK(id int, lv int) (*SpellIntro, bool) {
	obj := &SpellIntro{}
	sql := "select * from spell_intro where id =:id and lv =:lv "
	query := map[string]interface{}{
		"id": id,
		"lv": lv,
	}
	err := DBMap.SelectOne(obj, sql, query)
	if err != nil {
		log.Fatal(err)
		return obj, false
	}
	return obj, true
}

func (i *SpellIntro) Update() {
	_, err := DBMap.Update(i)
	if err != nil {
		log.Fatal(err)
	}
}

func InitDB() {
	once.Do(func() {
		db, err := sql.Open("mysql", "root:123456@tcp(127.0.0.1:3306)/sg_base")
		if err != nil {
			log.Fatal(err)
		}
		DBMap = &gorp.DbMap{
			Db:      db,
			Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"},
		}
		err = DBMap.Db.Ping()
		if err != nil {
			log.Fatal(err)
		}
	})
	DBMap.AddTableWithName(SpellIntro{}, "spell_intro").SetKeys(false, "id", "lv")
}

var count int

func ShowCount() {
	for {
		time.Sleep(time.Second)
		fmt.Println(count)
		count = 0
	}
}

func UpdateMysql() {
	spell, ok := GetByPK(2001, 1)
	if !ok {
		return
	}
	for {
		spell.Update()
		count++
	}
}
func main() {
	InitDB()
	go ShowCount()
	cnt, _ := strconv.Atoi(os.Args[1])
	for i := 0; i < cnt; i++ {
		go UpdateMysql()
	}
	time.Sleep(time.Hour)
}
