package main

import (
	"bufio"
	"fmt"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:9527")
	if err != nil {
		fmt.Println(err)
		return
	} else {
		fmt.Println("连接成功")
	}
	netRD := bufio.NewReader(conn)
	var c []byte
	for {
		b, err := netRD.ReadBytes(0x55)
		if err != nil {
			fmt.Println(err)
			continue
		}
		d, err := netRD.ReadByte()
		if d != 0xaa {
			c = append(c, b...)
			c = append(c, d)
			continue
		}
		if len(c) > 0 {
			b = append(c, b...)
			c = nil
		}
		b = b[:len(b)-1]
		fmt.Println(len(b))
	}
}
