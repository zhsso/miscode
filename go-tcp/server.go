package main

import (
	"fmt"
	"net"
	"time"
)

func dear10(conn net.Conn) {
	i := 0
	s := "helUlo woUrUld"
	for i := 0; i < 22; i++ {
		s = s + s
	}
	b := []byte(s)
	fmt.Println(len(b))
	for {

		b = append(b, 0x55)
		b = append(b, 0xaa)
		_, err := conn.Write(b)
		if err != nil {
			fmt.Println(err)
			conn.Close()
			break
		}
		i++
		fmt.Println(i)
		<-time.After(time.Second)
	}
}

func main() {
	ln, err := net.Listen("tcp", ":9527")
	if err != nil {
		fmt.Println(err)
		return
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}
		go dear10(conn)
	}
}
