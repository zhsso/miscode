package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"strings"
)

type Stream struct {
	IsStream   bool
	CarName    string
	RecordTime int64
	RtspUrl    string
}

type Streamslice struct {
	Streams []Stream
}

var ServerAddr = "rtsp://114.215.206.78:1935/vod/"

func getDuration(fileName string) int64 {
	cmd := exec.Command("ffmpeg", "-i", fileName)
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &out
	cmd.Run()
	b := out.Bytes()
	i := bytes.Index(b, []byte("Duration"))
	bi := b[i+10 : i+21]
	hours := int64((bi[0]-48)*10 + (bi[1] - 48))
	mins := int64((bi[3]-48)*10 + (bi[4] - 48))
	secs := int64((bi[6]-48)*10 + (bi[7] - 48))
	return hours*3600 + mins*60 + secs
}

//简单判断后缀名为mp4的即为视频文件
func isValid(fileName string) bool {
	if filepath.Ext(fileName) != ".mp4" {
		return false
	}
	return true
}

//有文件名+.tmp的为直播流文件
func isStream(fileName string) bool {
	pathtmp := fileName + ".tmp"
	_, err := os.Stat(pathtmp)
	return err == nil || os.IsExist(err)
}

func getRealName(fileName string) string {
	return fileName
}

//获得视频流的名称
func getCarName(fileName string) string {
	var tmp string
	a := strings.Split(filepath.Base(fileName), ".")
	if len(a) > 1 {
		a = a[:len(a)-1]
	}
	tmp = strings.Join(a, ".")

	b := strings.Split(tmp, "_")
	if !isStream(fileName) && len(b) > 1 {
		b = b[:len(b)-1]
	}
	tmp = strings.Join(b, "_")
	return getRealName(tmp)
}

//获得视频流地址
func genRtspUrl(fileName string) string {
	return ServerAddr + filepath.Base(fileName)
}

func getInfo(fileName string) (Sm Stream, isOk bool) {
	if !isValid(fileName) {
		isOk = false
		return
	}

	fileInfo, err := os.Stat(fileName)
	if err == nil {
		Sm.IsStream = isStream(fileName)
		Sm.CarName = getCarName(fileName)
		Mtime := reflect.ValueOf(fileInfo.Sys()).Elem().FieldByName("Mtim").Field(0).Int()
		Mtime = Mtime - getDuration(fileName)
		Sm.RecordTime = Mtime
		Sm.RtspUrl = genRtspUrl(fileName)
		isOk = true
	} else {
		isOk = false
	}
	return
}

func getVideoList(w http.ResponseWriter, r *http.Request) {

	var s Streamslice
	err := filepath.Walk(os.Args[1], func(fileName string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}
		if f.IsDir() {
			return nil
		}
		Sm, ok := getInfo(fileName)
		if ok {
			s.Streams = append(s.Streams, Sm)
		}
		return nil
	})

	if err != nil {
		fmt.Println("filepath.Walk() returned %v", err)
	}

	b, err := json.Marshal(s)
	if err != nil {
		fmt.Println("json err:", err)
	}
	fmt.Fprintf(w, string(b))
}

func main() {
	http.HandleFunc("/", getVideoList)
	err := http.ListenAndServe(":9527", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
