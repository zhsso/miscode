
wchar_t* UTF8ToUnicode(char* szU8, wchar_t* wszString) 
{
	//预转换，得到所需空间的大小 
	int wcsLen = ::MultiByteToWideChar(CP_UTF8, NULL, szU8, strlen(szU8), NULL, 0); 
	//转换 
	::MultiByteToWideChar(CP_UTF8, NULL, szU8, strlen(szU8), wszString, wcsLen); 
	//最后加上'\0' 
	wszString[wcsLen] = '\0'; 
	return wszString;
}
